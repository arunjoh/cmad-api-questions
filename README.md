# README #
This is a micro-service for the questions API as part of the CMAD Stackoverflow use case. 

## Build ##
We use maven to build. Running 'mvn clean install' will compile, test and build the project. The fat jar will be placed under the 'target' folder.
Docker: You can create a docker image by running the docker file in the root. In this case you can run the micro-service as a docker container. See the Dockerfile for further details. Note, the Dockerfile exposes port 8080.

## Run ## 
From the base git folder, run the fat jar as a java application. 
Option 1: To run with all defaults: 'java -jar target/stackoverflow-api-questions-fat.jar'. The application runs on port 8082.
Option 2: To use the config file (for example, to run on port 8080), run 'java -jar target/stackoverflow-api-questions-fat.jar -conf -conf src/main/resources/configuration/config.json'
Note, the json should have an attribute named 'httpPort' with the desired port number.

The following MongoDB parameters can be over-ridden by passing the relevant environment variables.
1. MongoDB database
	Environment variable: SO_MONGODB_DATABASE. Default value: cmad
2. MongoDB Connection URL
	Environment variable : SO_MONGODB_URL. Default value: mongodb://cmad-mongodb:27017

## API documentation ##
- Please refer to https://cmad-nard.restlet.io for the REST API documentation.
- If you have the application running locally, you can use http://localhost:8082/static/swagger-ui/index.html to try out the APIs.

## Deploy ##
This micro-service is part of the stackoverflow application. 
Please look into the cmad-deploy repository for details on running the complete application.


## TODO ##
- Validate Token and set user information into routing context for use by down-stream handlers.
- Schema validation: Generic implementation.
- Error handling: Test and enhance handler conditions in ExceptionHandler.js
- Testing:
  - Setup embedded mongodb for integration testing.
  - Setup to start Vert.x and run the application.
  - Create client jar from swagger definition.
  - Write a sample integration test case.